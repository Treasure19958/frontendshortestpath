package za.co.four.assignment.MxolisiMkhwanazi.service;

import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import za.co.four.assignment.MxolisiMkhwanazi.model.Route;


public interface RouteService {

    void saveRoue(MultipartFile file);
    List<Route> findAllRoutes();
}
