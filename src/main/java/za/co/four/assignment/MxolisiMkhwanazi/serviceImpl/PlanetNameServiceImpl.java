package za.co.four.assignment.MxolisiMkhwanazi.serviceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import za.co.four.assignment.MxolisiMkhwanazi.ExcelHelper.ExcelFileReaderHelper;
import za.co.four.assignment.MxolisiMkhwanazi.model.PlanetName;
import za.co.four.assignment.MxolisiMkhwanazi.repository.PlanetNameRepository;
import za.co.four.assignment.MxolisiMkhwanazi.service.PlanetNameService;

@Service
public class PlanetNameServiceImpl implements PlanetNameService {

    @Autowired
    PlanetNameRepository planetNameRepository;

    @Override
    public void savePlanetName(MultipartFile file) {
        try {
            List<PlanetName> planetNames = ExcelFileReaderHelper.readExcelToPlaneNames(file.getInputStream());
            planetNameRepository.saveAll(planetNames);
        } catch (IOException e) {
            throw new RuntimeException("fail to store excel data: " + e.getMessage());
        }
    }

    @Override
    public List<PlanetName> findAllPlanetNames() {
        return planetNameRepository.findAll();
    }
}