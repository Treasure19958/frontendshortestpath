package za.co.four.assignment.MxolisiMkhwanazi.service;


import java.util.List;

public interface ShortestPathService {

   List<String> getPath(String palnetOrigin, String planetDest);
}
