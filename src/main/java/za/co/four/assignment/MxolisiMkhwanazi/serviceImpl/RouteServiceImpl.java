package za.co.four.assignment.MxolisiMkhwanazi.serviceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import za.co.four.assignment.MxolisiMkhwanazi.ExcelHelper.ExcelFileReaderHelper;
import za.co.four.assignment.MxolisiMkhwanazi.model.Route;
import za.co.four.assignment.MxolisiMkhwanazi.repository.RouteRepository;
import za.co.four.assignment.MxolisiMkhwanazi.service.RouteService;

@Service
public class RouteServiceImpl implements RouteService {

    @Autowired
    RouteRepository routeRepository;

    @Override
    public void saveRoue(MultipartFile file) {
        try {
            List<Route> routes = ExcelFileReaderHelper.readExcelToRoutes(file.getInputStream());
            routeRepository.saveAll(routes);
        } catch (IOException e) {
            throw new RuntimeException("fail to store excel data: " + e.getMessage());
        }
    }

    @Override
    public List<Route> findAllRoutes() {
        return routeRepository.findAll();
    }
}
