package za.co.four.assignment.MxolisiMkhwanazi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import za.co.four.assignment.MxolisiMkhwanazi.model.PlanetName;

public interface PlanetNameRepository extends JpaRepository<PlanetName, String> {

}
