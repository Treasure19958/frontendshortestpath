package za.co.four.assignment.MxolisiMkhwanazi.ExcelHelper;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import za.co.four.assignment.MxolisiMkhwanazi.model.PlanetName;
import za.co.four.assignment.MxolisiMkhwanazi.model.Route;

public class ExcelFileReaderHelper {

    public static String TYPE = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";

    public static boolean hasExcelFormat(MultipartFile file) {

        if (!TYPE.equals(file.getContentType())) {
            return false;
        }

        return true;
    }

    public static List<PlanetName> readExcelToPlaneNames(InputStream is) {

        try {
            Workbook workbook = new XSSFWorkbook(is);
            Sheet sheet = workbook.getSheetAt(0);
            Iterator<Row> rows = sheet.iterator();

            List<PlanetName> planetNames = new ArrayList<PlanetName>();

            int rowNumber = 0;
            while (rows.hasNext()) {
                Row currentRow = rows.next();

                // skip header
                if (rowNumber == 0) {
                    rowNumber++;
                    continue;
                }

                Iterator<Cell> cellsInRow = currentRow.iterator();

                PlanetName planetName = new PlanetName();

                int cellIdx = 0;
                while (cellsInRow.hasNext()) {
                    Cell currentCell = cellsInRow.next();

                    switch (cellIdx) {
                        case 0:
                            planetName.setPlanetNode(currentCell.getStringCellValue());
                            break;

                        case 1:
                            planetName.setPlanetName(currentCell.getStringCellValue());
                            break;

                        default:
                            break;
                    }

                    cellIdx++;
                }

                planetNames.add(planetName);
            }

            ((XSSFWorkbook) workbook).close();

            return planetNames;

        } catch (IOException e) {
            throw new RuntimeException("fail to parse Excel file: " + e.getMessage());
        }
    }

    public static List<Route> readExcelToRoutes(InputStream is) {

        try {
            Workbook workbook = new XSSFWorkbook(is);
            Sheet sheet = workbook.getSheetAt(1);
            Iterator<Row> rows = sheet.iterator();

            List<Route> routes = new ArrayList<Route>();

            int rowNumber = 0;
            while (rows.hasNext()) {
                Row currentRow = rows.next();

                // skip header
                if (rowNumber == 0) {
                    rowNumber++;
                    continue;
                }

                Iterator<Cell> cellsInRow = currentRow.iterator();

                Route route = new Route();

                int cellIdx = 0;
                while (cellsInRow.hasNext()) {
                    Cell currentCell = cellsInRow.next();

                    switch (cellIdx) {
                        case 0:
                            route.setRouteId((int) currentCell.getNumericCellValue());
                            break;

                        case 1:
                            route.setPlanetOrigin(currentCell.getStringCellValue());
                            break;

                        case 2:
                            route.setPlanetDestination(currentCell.getStringCellValue());
                            break;

                        case 3:
                            route.setDistance(currentCell.getNumericCellValue());
                            break;

                        default:
                            break;
                    }

                    cellIdx++;
                }

                routes.add(route);
            }

            ((XSSFWorkbook) workbook).close();

            return routes;

        } catch (IOException e) {
            throw new RuntimeException("fail to parse Excel file: " + e.getMessage());
        }
    }
}