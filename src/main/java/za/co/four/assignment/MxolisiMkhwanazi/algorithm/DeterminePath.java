package za.co.four.assignment.MxolisiMkhwanazi.algorithm;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import za.co.four.assignment.MxolisiMkhwanazi.model.Graph;
import za.co.four.assignment.MxolisiMkhwanazi.model.PlanetName;
import za.co.four.assignment.MxolisiMkhwanazi.model.Route;

public class DeterminePath {

    private final List<PlanetName> planetNames;
    private final List<Route> routes;
    private Set<String> settledNodes;
    private Set<String> unSettledNodes;
    private Map<String, String> predecessors;
    private Map<String, Double> distance;

    public DeterminePath(Graph graph){
         this.planetNames = new ArrayList<PlanetName>(graph.getPlanetNames());
         this.routes = new ArrayList<Route>(graph.getRoutes());
    }

    public void execute(String planetOrigin){

        settledNodes = new HashSet<String>();
        unSettledNodes = new HashSet<String>();
        distance = new HashMap<String, Double>();
        predecessors = new HashMap<String, String>();
        distance.put(planetOrigin, 0.0);
        unSettledNodes.add(planetOrigin);
        while (unSettledNodes.size() > 0) {
            String node = getMinimum(unSettledNodes);
            settledNodes.add(node);
            unSettledNodes.remove(node);
            findMinimalDistances(node);
        }
    }

    private void findMinimalDistances(String node) {
        List<String> adjacentNodes = getNeighbors(node);
        for (String target : adjacentNodes) {
            if (getShortestDistance(target) > getShortestDistance(node)
                    + getDistance(node, target)) {
                distance.put(target, getShortestDistance(node)
                        + getDistance(node, target));
                predecessors.put(target, node);
                unSettledNodes.add(target);
            }
        }

    }

    private double getShortestDistance(String planetDest) {
        Double d = distance.get(planetDest);
        if (d == null) {
            return Double.MAX_VALUE;
        } else {
            return d;
        }
    }

    private double getDistance(String planetOrigin, String planetDest) {
        for (Route edge : routes) {
            if (edge.getPlanetOrigin().equals(planetOrigin)
                    && edge.getPlanetDestination().equals(planetDest)) {
                return edge.getDistance();
            }
        }
        throw new RuntimeException("Should not happen");
    }

    /*
     * This method returns the path from the source to the selected target and
     * NULL if no path exists
     */
    public LinkedList<String> getPath(String planetDest) {
        LinkedList<String> path = new LinkedList<String>();
        String step = planetDest;
        // check if a path exists
        if (predecessors.get(step) == null) {
            return null;
        }
        path.add(step);
        while (predecessors.get(step) != null) {
            step = predecessors.get(step);
            path.add(step);
        }
        // Put it into the correct order
        Collections.reverse(path);
        return path;
    }

    private List<String> getNeighbors(String node) {
        List<String> neighbors = new ArrayList<String>();
        for (Route edge : routes) {
            if (edge.getPlanetOrigin().equals(node)
                    && !isSettled(edge.getPlanetDestination())) {
                neighbors.add(edge.getPlanetDestination());
            }
        }
        return neighbors;
    }

    private boolean isSettled(String planetDestination) {
        return settledNodes.contains(planetDestination);
    }

    private String getMinimum(Set<String> planetNames) {
        String minimum = null;
        for (String planetName : planetNames) {
            if (minimum == null) {
                minimum = planetName;
            } else {
                if (getShortestDistance(planetName) < getShortestDistance(minimum)) {
                    minimum = planetName;
                }
            }
        }
        return minimum;
    }

}
