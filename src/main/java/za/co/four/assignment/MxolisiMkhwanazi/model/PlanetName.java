package za.co.four.assignment.MxolisiMkhwanazi.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Objects;

@Entity
@Table(name = "planetnames")
public class PlanetName {

    @Id
    @Column(name = "planetNode")
    private String planetNode;

    @Column(name = "planetName")
    private String planetName;

    public PlanetName(String planetNode, String planetName) {
        this.planetNode = planetNode;
        this.planetName = planetName;
    }

    public PlanetName() {

    }

    public String getPlanetNode() {
        return planetNode;
    }

    public void setPlanetNode(String planetNode) {
        this.planetNode = planetNode;
    }

    public String getPlanetName() {
        return planetName;
    }

    public void setPlanetName(String planetName) {
        this.planetName = planetName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PlanetName)) return false;
        PlanetName that = (PlanetName) o;
        return Objects.equals(getPlanetNode(), that.getPlanetNode()) &&
                Objects.equals(getPlanetName(), that.getPlanetName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getPlanetNode(), getPlanetName());
    }

    @Override
    public String toString() {
        return "PlanetName{" +
                "planetNode='" + planetNode + '\'' +
                ", planetName='" + planetName + '\'' +
                '}';
    }
}
