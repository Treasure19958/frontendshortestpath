import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';



export class RequestParam{

   constructor(public planetOrigin: string , 
                public planetDest: string){

   }
}

@Injectable({
  providedIn: 'root'
})
export class ShortestpathService {

  constructor(private http:HttpClient) { }
  
  getShortestPath(request){
      return this.http.post("http://localhost:8090/api/shortestpath/{planetOrigin}/{planetDest}", request);
  }
}
