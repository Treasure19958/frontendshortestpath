import { TestBed } from '@angular/core/testing';

import { ShortestpathService } from './shortestpath.service';

describe('ShortestpathService', () => {
  let service: ShortestpathService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ShortestpathService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
