import { ShortestpathService} from './../shortestpath.service';
import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { RequestParam} from './../shortestpath.service';



@Component({
  selector: 'app-shortestpath',
  templateUrl: './shortestpath.component.html',
  styleUrls: ['./shortestpath.component.css']
})

export class ShortestpathComponent implements OnInit {

  request: RequestParam = new RequestParam('', '');

 paths:any;
   
 form: FormGroup;
 createForm() {
 this.form = this.fb.group({
  oldPassphrase: ['', Validators.required],
  newPassphrase: ['', Validators.required]
  });
}

  constructor(private service:ShortestpathService, private fb: FormBuilder) {
    this.createForm();
   }

  ngOnInit(): void {
  
  }

  getShortest(){
   this.service.getShortestPath(this.request).subscribe(data =>{alert(JSON.stringify(data))})
  }

  onSubmit(){
    this.getShortest();
  }
}
